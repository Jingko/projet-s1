#ifndef DEF_CONSTANTES
#define DEF_CONSTANTES

	#define TAILLE_TABLEAU 22 /* Utilisé pour définir la taille du tableau */
	#define TAILLE_MAX 100

	#define NOIR "\033[00;30m"				/* Ceci est la liste       */
	#define BLEU "\033[00;34m"				/* des couleurs utilisées  */
	#define VERT "\033[00;32m"				/* pour afficher les       */
	#define CYAN "\033[00;36m"				/* différents caractères   */
	#define ROUGE "\033[00;31m"				/* présents sur le terrain */
	#define VIOLET "\033[00;35m"
	#define MARRON "\033[00;33m"
	#define GRIS_CLAIR "\033[00;37m"
	#define GRIS_FONCE "\033[01;30m"
	#define BLEU_CLAIR "\033[01;34m"
	#define VERT_CLAIR "\033[01;32m"
	#define CYAN_CLAIR "\033[01;36m"
	#define ROUGE_CLAIR "\033[01;31m"
	#define VIOLET_CLAIR "\033[01;35m"
	#define JAUNE "\033[01;33m"
	#define BLANC "\033[01;37m"

	enum {HAUT = 122, BAS = 115, DROITE = 100, GAUCHE = 113, QUITTER = 27, SAUVEGARDER = 109, TRIGGER_CHOIX = 112}; /* Directions possibles */
	enum {HERBE = 0, FLEUR = 1, ARBRE = 2, ROCHER = 3, CLEF = 4, PO = 5, CADENAS = 6, PIEGE = 7, MONSTRE = 8, EAU = 9, CADENAS_OUVERT = 10};
	/* différents "objets" présents sur le terrain ; PO = Pièce d'Or */
	enum {eventFLEUR, eventARBRE, eventROCHER, eventCLEF, eventPO, eventCADENAS, eventCADENAS_FERME, eventPIEGE, eventMONSTRE, eventEAU, eventOFF, eventKILL, eventBOUFFE};
	/* eventFLEUR doit prendre une valeur > 10 car sinon il y a un conflit entre l'enum des cases et l'enum des messages d'evenements */
#endif