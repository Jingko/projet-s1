#include <stdio.h>
#include <stdlib.h>
#include "constantes.h"

void editerCarte(char couleurPerso[12])
{
	int creationCarte[TAILLE_TABLEAU][TAILLE_TABLEAU], position[2];
	int i = 0, j = 0, creation = 1, choix = HERBE, choixActif = 0, nouveau = 0;
	char action = '\0';

	FILE *carte = NULL;

	position[0] = 0;
	position[1] = 0;

	printf("Voulez-vous editer sur une ancienne carte (1) ou en creer une nouvelle par-dessus l'autre (2) ? ");
	scanf("%d", &nouveau);

	if(nouveau == 1) // Si on choisit d'écrire par-dessus l'ancienne carte
	{
		carte = fopen("ter2.carte", "r"); // On l'ouvre et on la récupère dans creationCarte
		if(carte != NULL)
		{
			for(i=0 ; i<=TAILLE_TABLEAU-1 ; i++)
			{
				for (j=0 ; j<=TAILLE_TABLEAU-1 ; j++)
				{			
					creationCarte[i][j] = fgetc(carte) - 48;
				}

				fseek(carte, 1, SEEK_CUR);
			}

			fclose(carte);
		}
	}
	else if(nouveau == 2) // Si on écrit par-dessus l'ancienne carte
	{
		for(i=0 ; i<TAILLE_TABLEAU ; i++)
		{
			for(j=0 ; j<TAILLE_TABLEAU ; j++)
			{
				creationCarte[i][j] = HERBE; // On initialise creaionCarte avec de l'herbe partout
			}
		}
	}

	while(creation) // Boucle principale
	{
		system("clear");

		if(48 <= action && action <= 57)	/* Si on choisit un "objet" à placer (entre 48 et 57 car on recupère le caractère, */
		{									/* 0 est le 48ème caractère de la table ASCII et 9 en est le 57ème). Si on choisit */
			choix = action - 48;			/* 0 pour l'herbe, le bloc choisit correspondra à de l'Herbe                       */
		}

		if(choixActif == 1)
			creationCarte[position[0]][position[1]] = choix;

		for(i=0 ; i<=TAILLE_TABLEAU-1 ; i++)	// Affiche la carte et la légende
		{
			for (j=0 ; j<=TAILLE_TABLEAU-1 ; j++)
			{
				if(i == position[0] && j == position[1]) // On affiche un '[' à l'endroit où se trouve le joueur
					printf("%s[\033[0m", couleurPerso);
				else if(i == position[0] && j == position[1] + 1) // On affiche un ']' à la case suivant le joueur
					printf("%s]\033[0m", couleurPerso);
				else // On affiche un ' ' pour toutes les autres cases
					printf(" ");
				switch(creationCarte[i][j]) // Affiche les bons caractères en fonction de la case définie plus haut
				{
					case HERBE:
						printf("%s'\033[0m", VERT_CLAIR);
						break;
					case FLEUR:
						printf("%sœ\033[0m", VIOLET);
						break;
					case ARBRE:
						printf("%sY\033[0m", VERT);
						break;
					case ROCHER:
						printf("%sø\033[0m", GRIS_FONCE);
						break;
					case CLEF:
						printf("%sf\033[0m", GRIS_CLAIR);
						break;
					case PO:
						printf("%s°\033[0m", JAUNE);
						break;
					case CADENAS:
						printf("%s®\033[0m", MARRON);
						break;
					case CADENAS_OUVERT:
						printf("%so\033[0m", MARRON);
						break;
					case PIEGE:
						printf("%s@\033[0m", NOIR);
						break;
					case MONSTRE:
						printf("%sX\033[0m", ROUGE_CLAIR);
						break;
					case EAU:
						printf("%s~\033[0m", BLEU);
						break;
				}
			}

			if(i == position[0] && position[1] == TAILLE_TABLEAU - 1)
				printf("%s]\033[0m", couleurPerso);
			else
				printf(" ");
			
			if(i == choix + 6 && choixActif == 1) // on affiche une flèche à côté de la légende pour le choix correspondant
				printf("\t %s->\033[0m ", ROUGE);
			else if(i == choix + 6 && choixActif == 0)
				printf("\t -> ");
			else
				printf("\t    ");

			switch(i) // Affichage de la légende
			{
				case 6:
					printf("0.Herbe");
					break;
				case 7:
					printf("1.Fleur");
					break;
				case 8:
					printf("2.Arbre");
					break;
				case 9:
					printf("3.Rocher");
					break;
				case 10:
					printf("4.Clef");
					break;
				case 11:
					printf("5.PO");
					break;
				case 12:
					printf("6.Cadenas");
					break;
				case 13:
					printf("7.Piege");
					break;
				case 14:
					printf("8.Monstre");
					break;
				case 15:
					printf("9.Eau");
					break;
			}
			printf("\n");		
		}

		printf("Que voulez-vous faire ? ");
		scanf("%s", &action);

		switch(action) // Si on choisit de bouger le personnage, d'enregistrer ou de quitter
		{
			case HAUT:
				if(position[0] > 0)
					position[0] --;
				break;
			case BAS:
				if(position[0] < TAILLE_TABLEAU-1)
					position[0] ++;
				break;
			case DROITE:
				if(position[1] < TAILLE_TABLEAU-1)
					position[1] ++;
				break;
			case GAUCHE:
				if(position[1] > 0)
					position[1] --;
				break;
			case TRIGGER_CHOIX: //'p'
				choixActif = abs(choixActif - 1); // Permet de prendre les valeurs 0 et 1 successivement
				break;
			case SAUVEGARDER: //'m'
				carte = fopen("ter2.carte", "w+");
				if(carte != NULL)
				{
					for(i=0 ; i<=TAILLE_TABLEAU-1 ; i++)
					{
						for (j=0 ; j<=TAILLE_TABLEAU-1 ; j++)
						{
							fprintf(carte, "%d", creationCarte[i][j]);
						}
						if(i != TAILLE_TABLEAU-1)
							fprintf(carte, "\n");
					}
					fclose(carte);
				}
				creation = 0;
				break;
			case QUITTER:
				creation = 0;
				break;
		}
		
	}
}