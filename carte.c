#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include "constantes.h"

void afficherCarte(int carte[TAILLE_TABLEAU][TAILLE_TABLEAU], int position[2], char couleurPerso[12], int evenement[4]);
void creerFleur(int *randomTour, int *nombreTour, int *nouvelleFleur, int carte[TAILLE_TABLEAU][TAILLE_TABLEAU]);

int initialiserCarte(int carte[TAILLE_TABLEAU][TAILLE_TABLEAU]) /* Initialise la carte */
{
	int i = 0, j = 0, k = 0, choixCarte = 0, LargeurTableau = 0, HauteurTableau = 0, continuer = 1;
	char caractere = '\0', chaine[TAILLE_TABLEAU + 1] = {}; // Variables qui permettent de compter le nombre de lignes et de colonnes
	int longueurLigne[TAILLE_MAX] = {}; // Tableau qui permet de vérifier le nombre de colonnes de la carte pour chaque ligne

	printf("Choisissez la carte (1 ou 2) : "); // Demande du choix de carte
	scanf("%d", &choixCarte);

	FILE *terrain = NULL;

	if(choixCarte == 1)
		terrain = fopen("ter1.carte", "r"); // On prend la première carte si le joueur tape 1
	else if(choixCarte == 2)
		terrain = fopen("ter2.carte", "r"); // On prend la deuxième carte si le joueur tape 2
	else
	{
		terrain = NULL;	/* Sinon terrain ne vaut rien et on met continuer = 2 */
		continuer = 2;	/* pour que le message d'erreur de carte s'affiche    */
	}

	if(terrain != NULL) // Si la carte s'est correctement ouverte
	{
		rewind(terrain); // On se place au debut du fichier

		caractere = fgetc(terrain); // On prend le premier caractère

		rewind(terrain); // On se replace au début du fichier

		while(fgets(chaine, TAILLE_MAX, terrain) != NULL) // Tant qu'il reste une ligne à la fin
		{
			HauteurTableau ++; // On augmente le compteur de lignes
			if(HauteurTableau != TAILLE_TABLEAU) // Si ce compteur est différent du nombre fixé
				longueurLigne[k] = strlen(chaine) - 1; // La taille de la chaine est calculée (-1 car il y a le caractère '\n')
			else
				longueurLigne[k] = strlen(chaine); // Si on est à la dernière ligne, il n'y a pas de '\n'
			k++; // On incrémente k
		}

		for(k=0 ; k<=HauteurTableau-1 ; k++) // On va comparer la longueur d'une ligne (largeur) et la hauteur avec celles fixées
		{
        	if((longueurLigne[k] != TAILLE_TABLEAU) || (HauteurTableau != TAILLE_TABLEAU))
        		continuer = 2;
        	/* Si la longueur de chaque ligne est différente de celle fixée OU si la hauteur du tableau calculée est différente de celle fixée */
        	/* OU si la longueur de la première ligne est différente de la longueur fixée, on affiche le message d'erreur de carte             */
 		}

		rewind(terrain);

		for(i=0 ; i<=TAILLE_TABLEAU-1 ; i++)		/* Permet d'aller  */
		{											/* dans toutes les */
			for (j=0 ; j<=TAILLE_TABLEAU-1 ; j++)	/* cases une à une */
			{			
				carte[i][j] = fgetc(terrain) - 48;	/* Récupère les cararactère du fichier contenant la carte un à un. Il y a -48         */
													/* car dans la table ASCII, le caractère '0' correspond au n°48 (Merci Bastien ! :D)  */
			}
			fseek(terrain, 1, SEEK_CUR);
		}

		fclose(terrain);
	}
	
	return continuer;
}

void afficherCarte(int carte[TAILLE_TABLEAU][TAILLE_TABLEAU], int position[2], char couleurPerso[12], int evenement[5])
{
	/* Cette fonction permet d'afficher le nombre de vies, de clefs en notre possession, */
	/* de PO amassées et permet aussi d'afficher la carte en entier                      */
	
	int i = 0, j = 0, k = 0;

	system("clear"); /* "Efface la console" */

	for(i=0 ; i<=TAILLE_TABLEAU-1 ; i++)			/* Permet de se */
	{												/* ballader sur */
		for (j=0 ; j<=TAILLE_TABLEAU-1 ; j++)		/* la carte     */
		{
			if(i == position[0] && j == position[1]) /* Affiche le joueur à la position initiale */
				printf("%s&\033[0m ", couleurPerso);
			else
			{
				switch(carte[i][j]) // Affiche les bons caractères en fonction de la case définie plus haut
				{
					case HERBE:
						printf("%s'\033[0m ", VERT_CLAIR);
						break;
					case FLEUR:
						printf("%sœ\033[0m ", VIOLET);
						break;
					case ARBRE:
						printf("%sY\033[0m ", VERT);
						break;
					case ROCHER:
						printf("%sø\033[0m ", GRIS_FONCE);
						break;
					case CLEF:
						printf("%sf\033[0m ", GRIS_CLAIR);
						break;
					case PO:
						printf("%s°\033[0m ", JAUNE);
						break;
					case CADENAS:
						printf("%s®\033[0m ", MARRON);
						break;
					case CADENAS_OUVERT:
						printf("%so\033[0m ", MARRON);
						break;
					case PIEGE:
						printf("%s@\033[0m ", NOIR);
						break;
					case MONSTRE:
						printf("%sX\033[0m ", ROUGE_CLAIR);
						break;
					case EAU:
						printf("%s~\033[0m ", BLEU);
						break;
				}
			}
		}
		switch(i) // Permet d'afficher l'ATH (Affichage Tête Haute) en fonction de la ligne (i)
		{
			case 1:
				printf("\tVies :");
				break;
			case 2:
				printf("\t");
				for(k=1 ; k<=evenement[0] ; k++) // Affiche le nombre de vies restantes (1 caractère par vie)
					printf("%sæ\033[0m", ROUGE);
				break;
			case 3:
				printf("\tEnergie :");
				break;
			case 4:
				printf("\t");
				for(k=1 ; k<=evenement[4] ; k++) // Affiche le nombre d'énergie restantes (1 caractère par point d'énergie)
					printf("%s¶\033[0m", MARRON);
				break;
			case 5:
				printf("\tPieces :");
				break;
			case 6:
				printf("\t");
				for(k=1 ; k<=evenement[2] ; k++) // Affiche le nombre de pièces amassées (1 caractère par PO)
					printf("%s°\033[0m", JAUNE);
				break;
			case 7:
				printf("\tClefs :");
				break;
			case 8:
				printf("\t");
				for(k=1 ; k<=evenement[1] ; k++) // Affiche le nombre de clefs amassées (1 caractère par clef)
					printf("%sf\033[0m", GRIS_CLAIR);
				break;
		}
		printf("\n");
	}
}

void creerFleur(int *randomTourFleur, int *nombreTourFleur, int *nouvelleFleur, int carte[TAILLE_TABLEAU][TAILLE_TABLEAU])
{
	/* Cette fonction permet de créer des fleurs de façon aléatoire */

	int i = 0, j = 0;
	int positionFleur[2];

	positionFleur[0] = 0;
	positionFleur[1] = 0;

	*nouvelleFleur = 1; // La fleur est en création
	
	if(*nombreTourFleur == *randomTourFleur) // Si on a fait un nombre de tour égal à celui choisi au hasard
	{
		positionFleur[0] = (rand() % (TAILLE_TABLEAU)); /* On prend une position au */
		positionFleur[1] = (rand() % (TAILLE_TABLEAU));	/* hasard sur x et sur y    */

		while(carte[positionFleur[0]][positionFleur[1]] != HERBE) // Tant que cette case n'est pas de l'herbe, on en prend une autre au hasard
		{
			positionFleur[0] = (rand() % (TAILLE_TABLEAU));
			positionFleur[1] = (rand() % (TAILLE_TABLEAU));
		}
		
		carte[positionFleur[0]][positionFleur[1]] = FLEUR; // On remplace la case par une fleur
		*nombreTourFleur = 0; // On réinitialise nombreTour
		*nouvelleFleur = 0; // On dit qu'une fleur vient d'être créée
	}
}