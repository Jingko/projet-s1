#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "constantes.h"

int main(int argc, char *argv[])
{
	int carte[TAILLE_TABLEAU][TAILLE_TABLEAU]; /* Carte */
	int position[2]; /* Position du joueur */
	int continuer = 1, nombreTourFleur = 0, randomTourFleur = 0, nouvelleFleur = 0, nombreTourEnergie = 0, randomTourEnergie = 0, perteEnergie = 0;
	char couleurPerso[12] = CYAN_CLAIR; /* Initialisation de la couleur du perso */
	int evenement[5]; /* Stocke les valeurs du nombre de vies, clefs, po */
	int question = 0, nombrePO = 0, i = 0, j = 0;

	evenement[0] = 10; /* Correspond au nombre de vies */
	evenement[1] = 0; /* Correspond au nombre de clefs */
	evenement[2] = 0; /* Correspond au nombre de PO */
	evenement[3] = eventOFF; /* Correspond au message de collision */
	evenement[4] = 10; /* Correspond à l'énergie du personnage */

	position[0] = 1;
	position[1] = 1;

	srand(time(NULL));

	printf("Voulez-vous jouer ou créer une carte (1 pour jouer, 2 pour creer) ? ");
	scanf("%d", &question);

	if(question == 2)
	{
		editerCarte(couleurPerso);
		printf("Fin de creation de carte !\n\n");
	}

	else if(question == 1)
	{

		continuer = initialiserCarte(carte);	/* Initialise la carte en récupérant le fichier contenant la carte, la vérifie */
												/* et récupère le message d'erreur éventuel (grâce à la variable continuer)    */               

		for(i=0 ; i<TAILLE_TABLEAU ; i++)
		{
			for(j=0 ; j<TAILLE_TABLEAU ; j++)
			{
				if(carte[i][j] == PO) // Compte le nombre pièces totales
					nombrePO ++;
			}
		}

		while(continuer == 1) /* Boucle principale */
		{
			if(nouvelleFleur == 0) // Si une nouvelle fleur vient d'être créée
				randomTourFleur = (rand() % 15) + 2; // On prend un nombre au hasard entre 2 et 16

			if(perteEnergie == 0) // Si on vient de perdre de l'énergie
			{
				randomTourEnergie = (rand() % 3) + 7; // On prend un nombre au hasard entre 7 et 9
				perteEnergie = 1;
			}

			creerFleur(&randomTourFleur, &nombreTourFleur, &nouvelleFleur, carte); // Cette fonction permet de créer une fleur de façon aléatoire

			afficherCarte(carte, position, couleurPerso, evenement); /* Affiche la carte */
			continuer = deplacerPersonnage(position, carte, evenement, couleurPerso);
			/* Deplace le personnage */

			nombreTourFleur ++; // Permet de compter depuis combien de tours la dernière fleur a été créée
			nombreTourEnergie ++; // Compte le nombre de tours fait depuis la dernière perte d'énergie

			if(nombreTourEnergie == randomTourEnergie && evenement[4] > 0) // Si on a fait un nombre de tour égal à celui choisi au hasard
			{
				perteEnergie = 0; // On indique qu'il faudra reprendre un nombre au hasard
				evenement[4] --; // On diminue de 1 la barre d'énergie
				nombreTourEnergie = 0; // On réinitialise le compteur de tours
			}

			if(evenement[4] == 0) // Si le joueur n'a plus d'énergie
				evenement[0] --; // Il perd une vie à chaque tour

			if(evenement[0] <= 0) // Si le joueur est mort
			{
				afficherCarte(carte, position, couleurPerso, evenement);
				printf("Fin de la partie, vous avez PERDU avec %d PO\n\n", evenement[2]); /* Message de fin de partie */
				continuer = 0; /* Fin de la boucle */
			}
			else if(evenement[2] == nombrePO) /* Si le joueur a toutes les PO */
			{
				afficherCarte(carte, position, couleurPerso, evenement);
				printf("Fin de la partie, vous avez GAGNE avec %d vie%c\n\n", evenement[0], evenement[0] > 1 ? 's' : ' '); /* Message de fin de partie */
				continuer = 0;
			}
			else if(continuer == 0) /* Si le joueur quitte la partie en cours */
				printf("Fin de la partie !\n\n"); /* Message de fin de partie */
		}

		if(continuer == 2) // Message d'erreur si la carte est mal configurée (voir fonction initialiserCarte)
		printf("Erreur de carte !\n");
	}

	return 0;
}