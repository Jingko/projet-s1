#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "constantes.h"

void collision(int positionDebut[2], int positionFin[2], int carte[TAILLE_TABLEAU][TAILLE_TABLEAU], int evenement[5], char couleurPerso[12]);

int deplacerPersonnage(int positionDebut[2], int carte[TAILLE_TABLEAU][TAILLE_TABLEAU], int evenement[5], char couleurPerso[12])
{
	char direction = '\0'; // Indique la direction
	int positionFin[2]; // Variable utile pour tester la collision avec le décor

	switch(evenement[3]) // Affiche un message en fonction de l'evenement produit
	{
		case eventFLEUR:
			printf("Vous ecrasez une fleur :(\n");
			break;
		case eventARBRE:
			printf("Un arbre bloque votre chemin. Contournez-le\n");
			break;
		case eventROCHER:
			printf("Un rocher vous bloque\n");
			break;
		case eventCLEF:
			printf("Vous recuperez une clef\n");
			break;
		case eventPO:
			printf("Vous ramassez une pièce\n");
			break;
		case eventCADENAS:
			printf("Vous ouvrez le cadenas a l'aide d'une de vos clefs\n");
			break;
		case eventCADENAS_FERME:
			printf("Vous n'avez pas de clef pour ouvrir le cadenas\n");
			break;
		case eventPIEGE:
			printf("Vous tombez dans un piege et vous perdez une vie :(\n");
			break;
		case eventMONSTRE:
			printf("Vous combattez un monstre mais vous perdez une vie car vous etes mauvais\n");
			break;
		case eventKILL:
			printf("Vous tuez un monstre et sortez du combat indemne\n");
			break;
		case eventBOUFFE:
			printf("Vous tuez le monstre et recuperez de l'energie en le mangeant\n");
			break;
		case eventEAU:
			printf("Vous tombez a l'eau et revenez sur le rivage mais perdez une vie\n");
			break;
		case eventOFF:
			break;
	}

	printf("Dans quelle direction voulez-vous aller ? : ");
	scanf("%s", &direction); /* On demande la direction */
	
	switch(direction) /* On vérifie quelle direction le joueur a demandé et on agit en fonction de ça */
	{
		case HAUT: /* Si le joueur va en haut */
			positionFin[0] = positionDebut[0] - 1; /* La position finale est une case au-dessus de la position initiale */
			positionFin[1] = positionDebut[1];
			collision(positionDebut, positionFin, carte, evenement, couleurPerso); /* On gère la collision */
			break;
		case BAS: /* Si le joueur va en bas */
			positionFin[0] = positionDebut[0] + 1; /* La position finale est une case en-dessous de la position initiale */
			positionFin[1] = positionDebut[1];
			collision(positionDebut, positionFin, carte, evenement, couleurPerso);
			break;
		case DROITE: /* Si le joueur va à droite */
			positionFin[0] = positionDebut[0];
			positionFin[1] = positionDebut[1] + 1; /* La position finale est une case à droite de la position initiale */
			collision(positionDebut, positionFin, carte, evenement, couleurPerso);
			break;
		case GAUCHE: /* Si le joueur va à gauche */
			positionFin[0] = positionDebut[0];
			positionFin[1] = positionDebut[1] - 1; /* La position finale est une case à gauche de la position initiale */
			collision(positionDebut, positionFin, carte, evenement, couleurPerso);
			break;
		case QUITTER: /* Si le joueur demande à quitter la partie */
			return 0; /* On met 0 dans la variable continuer pour quitter la boucle While du main */
			break;
	}
	printf("\n");
	return 1;
}

void collision(int positionDebut[2], int positionFin[2], int carte[TAILLE_TABLEAU][TAILLE_TABLEAU], int evenement[5], char couleurPerso[12])
{
	/*
	Permet de gérer la collision en demandant la position initiale, celle finale,
	la carte si il y a besoin de la modifier, le compteur de vies, de clefs et de PO,
	ainsi que la couleur du personnage pour la changer si besoin est.
	*/

	int randomKill = 0, randomBouffe = 0;

	sprintf(couleurPerso, CYAN_CLAIR); /* Mettre le personnage en couleur CYAN_CLAIR ("constantes.h") */

	if((positionFin[0] >= 0) && (positionFin[0] <= TAILLE_TABLEAU - 1) && (positionFin[1] >= 0) && (positionFin[1] <= TAILLE_TABLEAU - 1))
	{

		switch(carte[positionFin[0]][positionFin[1]]) /* Gestion d'évènements (en fonction de où le personnage doit aller (position finale)) */
		{
			case HERBE:
				positionDebut[0] = positionFin[0]; // Si la case désignée par le joueur est de l'herbe, on avance le personnage
				positionDebut[1] = positionFin[1];
				evenement[3] = eventOFF;
				break;
			case FLEUR:
				carte[positionFin[0]][positionFin[1]] = HERBE;	/* Si le joueur marche sur une fleur, la fleur est remplacée */
				positionDebut[0] = positionFin[0];				/* par de l'herbe et on fait avancer le personnage           */
				positionDebut[1] = positionFin[1];
				evenement[3] = eventFLEUR;
				break;
			case ARBRE:
				evenement[3] = eventARBRE; // Si la case suivante est un arbre, on affiche le message correspondant
				break;
			case ROCHER:
				evenement[3] = eventROCHER; // Idem pour le rocher
				break;
			case CLEF:
				evenement[1] ++;
				carte[positionFin[0]][positionFin[1]] = HERBE;	/* Si le joueur marche sur une clef, la clef est remplacée */
				positionDebut[0] = positionFin[0];				/* par de l'herbe, on fait avancer le personnage et        */
				positionDebut[1] = positionFin[1];				/* on augmente le compteur de clef de 1                    */
				evenement[3] = eventCLEF;
				break;
			case PO:
				evenement[2] ++;
				carte[positionFin[0]][positionFin[1]] = HERBE;	/* Si le joueur marche sur une PO, la PO est remplacée */
				positionDebut[0] = positionFin[0];				/* par de l'herbe, on fait avancer le personnage et    */
				positionDebut[1] = positionFin[1];				/* on augmente le compteur de PO de 1                  */
				evenement[3] = eventPO;
				break;
			case CADENAS:
				if(evenement[1] >= 1)										/* Si le joueur veut passer par un cadenas et qu'il possède */
				{															/* au moins une clef, on le fait avancer, on remplace le    */
					evenement[1] --;										/* cadenas par un cadenas ouvert, et on diminue le nombre   */
					carte[positionFin[0]][positionFin[1]] = CADENAS_OUVERT;	/* de clef de 1. Si le joueur ne possède pas de clefs,      */
					positionDebut[0] = positionFin[0];						/* le cadenas reste fermé                                   */
					positionDebut[1] = positionFin[1];
					evenement[3] = eventCADENAS;
				}
				else
					evenement[3] = eventCADENAS_FERME;
				break;
			case CADENAS_OUVERT: /* Si la case désignée par le joueur est un cadenas ouvert, on avance le personnage */
				positionDebut[0] = positionFin[0];
				positionDebut[1] = positionFin[1];
				break;
			case PIEGE:												
				evenement[0] --;									/* Si le joueur tombe dans le piège, il perd une vie,    */
				carte[positionFin[0]][positionFin[1]] = HERBE;		/* le personnage se déplace dans le piège en prenant     */
				positionDebut[0] = positionFin[0];					/* la couleur de celui-ci (NOIR) et on raffiche la carte */
				positionDebut[1] = positionFin[1];					/* avec la bonne couleur, puis le piège disparaît et     */
				sprintf(couleurPerso, NOIR);						/* est remplacé par de l'herbe                           */
				afficherCarte(carte, positionDebut, couleurPerso, evenement);
				evenement[3] = eventPIEGE;
				break;
			case MONSTRE:
				evenement[0] --;									/* Si le joueur va sur une case monstre, il perd une vie  */
				positionDebut[0] = positionFin[0];					/* et on fait avancer le personnage en prenant la couleur */
				positionDebut[1] = positionFin[1];					/* de celui-ci (ROUGE CLAIR) et on raffiche la carte avec */
				sprintf(couleurPerso, ROUGE_CLAIR);					/* la bonne couleur du personnage                         */
				afficherCarte(carte, positionDebut, couleurPerso, evenement);
				randomKill = rand() % 2;
				if(randomKill == 1) // Si on tue l'ennemi
				{
					carte[positionFin[0]][positionFin[1]] = HERBE; // La case est remlacée par de l'herbe
					randomBouffe = (rand() % 3) + 1; // On gagne entre 1 et 3 points d'énergie
					evenement[4] += randomBouffe;
					if(evenement[4] > 10) // Si l'énergie totale dépasse 10, on la met à 10
						evenement[4] = 10;
					if(randomBouffe == 0) // Si on gagne 0 points d'énergie, on affiche le message correspondant
						evenement[3] = eventKILL;
					else // Sinon on affiche l'autre message
						evenement[3] = eventBOUFFE;
				}
				else // Sinon on affiche le message de défaite du combat
					evenement[3] = eventMONSTRE;
				break;
			case EAU:
				sprintf(couleurPerso, BLEU);								/* Si le joueur tombre dans l'eau, le joueur prend la    */
				afficherCarte(carte, positionFin, couleurPerso, evenement);	/* couleur BLEU, on raffiche la carte avec la nouvelle   */
				sleep(1);													/* couleur et on attend 1 seconde pour que le personnage */
				sprintf(couleurPerso, CYAN_CLAIR);							/* se repositionne sur le rivage avec la bonne couleur   */
				evenement[3] = eventEAU;									/* (CYAN CLAIR)                                          */
				break;
		}
	}
}